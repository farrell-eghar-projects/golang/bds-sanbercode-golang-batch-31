package middlewares

import (
	"Final-Project/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func RolePermissionValidation(c *gin.Context) (int, gin.H) {
	user := models.LoggedInUser

	if !user.Role.RoleWritePermission {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if c.Param("roleID") != "" {
			conv, _ := strconv.Atoi(c.Param("roleID"))
			if user.RoleID == uint(conv) {
				return http.StatusUnauthorized, gin.H{"error": "Cannot edit your own Role!"}
			}
		}

		return http.StatusOK, gin.H{"success": "You are Authorized!"}
	}
}

func UserPermissionValidation(c *gin.Context) (int, gin.H) {
	var target models.User
	user := models.LoggedInUser

	if !user.Role.UserWritePermission {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if c.Param("userID") != "" {
			db := c.MustGet("db").(*gorm.DB)
			conv, _ := strconv.Atoi(c.Param("userID"))
			if err := db.Preload("Role").First(&target, conv).Error; err != nil {
				return http.StatusBadRequest, gin.H{"error": "Record not found!"}
			}

			if (user.RoleID == target.RoleID && !user.Leader) || target.Role.RoleWritePermission {
				return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
			} else if user.ID == uint(conv) {
				return http.StatusUnauthorized, gin.H{"error": "Cannot proceed your own Account!"}
			}
		}

		return http.StatusOK, gin.H{"success": "You are Authorized!"}
	}
}

// Hanya user dengan UserWritePermission atau Leader dari Developer / Publisher
// Selain kasus POST, Leader hanya dapat Update dan Delete Game milik perusahaannya
func GamePermissionValidation(c *gin.Context) (int, gin.H) {
	user := models.LoggedInUser
	var userDevs []models.Developer
	var userPubs []models.Publisher
	var games []models.Game

	if user.Role.UserWritePermission {
		return http.StatusOK, gin.H{"success": "You are Authorized!"}
	} else if !user.Role.UserWritePermission && !user.Leader {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if c.Param("gameID") != "" {
			db := c.MustGet("db").(*gorm.DB)
			conv, _ := strconv.Atoi(c.Param("gameID"))
			if err := db.Model(&user).Association("Developers").Find(&userDevs); err != nil {
				if err := db.Model(&user).Association("Publishers").Find(&userPubs); err != nil {
					return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
				} else {
					for _, userPub := range userPubs {
						if err := db.Model(&userPub).Association("Games").Find(&games); err == nil {
							for _, game := range games {
								if game.ID == uint(conv) {
									return http.StatusOK, gin.H{"success": "You are Authorized!"}
								}
							}
						}
					}
				}
			} else {
				for _, userDev := range userDevs {
					if err := db.Model(&userDev).Association("Games").Find(&games); err == nil {
						for _, game := range games {
							if game.ID == uint(conv) {
								return http.StatusOK, gin.H{"success": "You are Authorized!"}
							}
						}
					}
				}
			}

			return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
		}

		return http.StatusOK, gin.H{"success": "You are Authorized!"}
	}
}

func ReviewPermissionValidation(c *gin.Context) (int, gin.H) {
	user := models.LoggedInUser
	var target models.Review

	if !user.Role.ReviewWritePermission {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if c.Param("reviewID") != "" {
			db := c.MustGet("db").(*gorm.DB)
			conv, _ := strconv.Atoi(c.Param("reviewID"))
			if err := db.First(&target, conv).Error; err != nil {
				return http.StatusBadRequest, gin.H{"error": "Record not found!"}
			}

			if target.UserID != user.ID {
				return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
			}
		}

		return http.StatusOK, gin.H{"success": "You are Authorized!"}
	}
}

func MyGamePermissionValidation(c *gin.Context) (int, gin.H) {
	user := models.LoggedInUser
	var target models.MyGame

	if !user.Role.MyGameWritePermission {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if c.Param("myGameID") != "" {
			db := c.MustGet("db").(*gorm.DB)
			conv, _ := strconv.Atoi(c.Param("myGameID"))
			if err := db.First(&target, conv).Error; err != nil {
				return http.StatusBadRequest, gin.H{"error": "Record not found!"}
			}

			if target.UserID != user.ID {
				return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
			}
		}
	}

	return http.StatusOK, gin.H{"success": "You are Authorized!"}
}

func WishlistPermissionValidation(c *gin.Context) (int, gin.H) {
	user := models.LoggedInUser
	var target models.Wishlist

	if !user.Role.WishlistWritePermission {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if c.Param("myGameID") != "" {
			db := c.MustGet("db").(*gorm.DB)
			conv, _ := strconv.Atoi(c.Param("myGameID"))
			if err := db.First(&target, conv).Error; err != nil {
				return http.StatusBadRequest, gin.H{"error": "Record not found!"}
			}

			if target.UserID != user.ID {
				return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
			}
		}
	}

	return http.StatusOK, gin.H{"success": "You are Authorized!"}
}

func DeveloperPermissionValidation(c *gin.Context) (int, gin.H) {
	user := models.LoggedInUser
	var target []models.Developer

	if !user.Role.DeveloperWritePermission {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if !user.Leader {
			return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
		}

		db := c.MustGet("db").(*gorm.DB)
		conv, _ := strconv.Atoi(c.Param("devID"))
		if err := db.Model(&user).Association("Developers").Find(&target, conv); err != nil {
			return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
		}
	}

	return http.StatusOK, gin.H{"success": "You are Authorized!"}
}

func PublisherPermissionValidation(c *gin.Context) (int, gin.H) {
	user := models.LoggedInUser
	var target []models.Publisher

	if !user.Role.PublisherWritePermission {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if !user.Leader {
			return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
		}

		db := c.MustGet("db").(*gorm.DB)
		conv, _ := strconv.Atoi(c.Param("pubID"))
		if err := db.Model(&user).Association("Publishers").Find(&target, conv); err != nil {
			return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
		}
	}

	return http.StatusOK, gin.H{"success": "You are Authorized!"}
}

func DevMemberPermissionValidation(c *gin.Context) (int, gin.H) {
	user := models.LoggedInUser

	if !user.Role.DeveloperMemberWritePermission {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if c.Param("devMemberID") != "" {
			if !user.Leader {
				return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
			}
		}
	}

	return http.StatusOK, gin.H{"success": "You are Authorized!"}
}

func PubMemberPermissionValidation(c *gin.Context) (int, gin.H) {
	user := models.LoggedInUser

	if !user.Role.PublisherMemberWritePermission {
		return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
	} else {
		if c.Param("pubMemberID") != "" {
			if !user.Leader {
				return http.StatusUnauthorized, gin.H{"error": "You are Unauthorized!"}
			}
		}
	}

	return http.StatusOK, gin.H{"success": "You are Authorized!"}
}
