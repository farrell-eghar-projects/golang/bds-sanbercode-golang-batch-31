package models

import "time"

type (
	GameDeveloper struct {
		ID          uint      `json:"id" gorm:"primary_key"`
		GameID      uint      `json:"gameID"`
		DeveloperID uint      `json:"developerID"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
		Game        Game      `json:"-"`
		Developer   Developer `json:"-"`
	}
)
