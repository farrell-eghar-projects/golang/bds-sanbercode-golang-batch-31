package models

import (
	"Final-Project/utils/token"
	"html"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type (
	User struct {
		ID               uint              `json:"id" gorm:"primary_key"`
		RoleID           uint              `json:"roleID"`
		Leader           bool              `json:"leader"`
		Fullname         string            `json:"fullname"`
		Nickname         string            `json:"nickname"`
		Email            string            `json:"email"`
		Username         string            `json:"username" gorm:"not null;unique"`
		Password         string            `json:"password"`
		Age              int               `json:"age"`
		Gender           string            `json:"gender"`
		DateOfBirth      time.Time         `json:"date_of_birth"`
		CreatedAt        time.Time         `json:"created_at"`
		UpdatedAt        time.Time         `json:"updated_at"`
		Role             Role              `json:"-"`
		MyGames          []MyGame          `json:"-"`
		Wishlists        []Wishlist        `json:"-"`
		ReviewComments   []ReviewComment   `json:"-"`
		ReviewLikes      []ReviewLike      `json:"-"`
		DeveloperMembers []DeveloperMember `json:"-"`
		PublisherMembers []PublisherMember `json:"-"`
	}
)

var LoggedInUser User

func VerifyPassword(password, hasedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hasedPassword), []byte(password))
}

func LoginCheck(username, password string, db *gorm.DB) (string, error) {
	var err error
	u := User{}
	err = db.Model(User{}).Where("username = ?", username).Preload("Role").Take(&u).Error
	if err != nil {
		return "", err
	}

	err = VerifyPassword(password, u.Password)
	if err != nil && err == bcrypt.ErrMismatchedHashAndPassword {
		return "", err
	}

	token, err := token.GenerateToken(u.ID)
	if err != nil {
		return "", err
	}

	LoggedInUser = u

	return token, nil
}

func (u *User) SaveUser(db *gorm.DB) (*User, error) {
	hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if errPassword != nil {
		return &User{}, errPassword
	}

	u.Password = string(hashedPassword)
	u.Username = html.EscapeString(strings.TrimSpace(u.Username))
	if err := db.Create(&u).Error; err != nil {
		return &User{}, err
	}

	return u, nil
}

func (u *User) ChangePassowrd(db *gorm.DB) (*User, error) {
	hashedPassword, errPassword := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)
	if errPassword != nil {
		return &User{}, errPassword
	}

	u.Password = string(hashedPassword)
	if err := db.Model(&LoggedInUser).Updates(&u).Error; err != nil {
		return &User{}, err
	}

	return &LoggedInUser, nil
}
