package models

import "time"

type (
	Review struct {
		ID             uint            `json:"id" gorm:"primary_key"`
		UserID         uint            `json:"userID"`
		GameID         uint            `json:"gameID"`
		Title          string          `json:"title"`
		Description    string          `json:"description"`
		CreatedAt      time.Time       `json:"created_at"`
		UpdatedAt      time.Time       `json:"updated_at"`
		User           User            `json:"-"`
		Game           Game            `json:"-"`
		ReviewComments []ReviewComment `json:"-"`
		ReviewLikes    []ReviewLike    `json:"-"`
	}
)
