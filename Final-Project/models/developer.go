package models

import "time"

type (
	Developer struct {
		ID               uint              `json:"id" gorm:"primary_key"`
		Name             string            `json:"name"`
		Website          string            `json:"website"`
		Twitter          string            `json:"twitter"`
		CreatedAt        time.Time         `json:"created_at"`
		UpdatedAt        time.Time         `json:"updated_at"`
		GameDevelopers   []GameDeveloper   `json:"-"`
		DeveloperMembers []DeveloperMember `json:"-"`
	}
)
