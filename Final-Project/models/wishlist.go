package models

import "time"

type (
	Wishlist struct {
		ID        uint      `json:"id" gorm:"primary_key"`
		UserID    uint      `json:"userID"`
		GameID    uint      `json:"gameID"`
		CreatedAt time.Time `json:"created_at"`
		UpdatedAt time.Time `json:"updated_at"`
		User      User      `json:"-"`
		Game      Game      `json:"-"`
	}
)
