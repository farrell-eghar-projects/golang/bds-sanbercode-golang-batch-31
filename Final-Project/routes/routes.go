package routes

import (
	"Final-Project/controllers"
	"Final-Project/middlewares"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gorm.io/gorm"
)

func SetupRouter(db *gorm.DB) *gin.Engine {
	r := gin.Default()
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
	})

	// Router Grouping
	jwtAuthMiddlewareRoute := r.Group("/")
	roleMiddlewareRoute := r.Group("/")
	userMiddlewareRoute := r.Group("/")
	gameMiddlewareRoute := r.Group("/")
	reviewMiddlewareRoute := r.Group("/")
	myGameMiddlewareRoute := r.Group("/")
	wishlistMiddlewareRoute := r.Group("/")
	developerMiddlewareRoute := r.Group("/")
	publisherMiddlewareRoute := r.Group("/")
	devMemberMiddlewareRoute := r.Group("/")
	pubMemberMiddlewareRoute := r.Group("/")

	// Router Middleware
	jwtAuthMiddlewareRoute.Use(middlewares.JWTAuthMiddleware())
	roleMiddlewareRoute.Use(middlewares.RolePermissionMiddleware())
	userMiddlewareRoute.Use(middlewares.UserPermissionMiddleware())
	gameMiddlewareRoute.Use(middlewares.GamePermissionMiddleware())
	reviewMiddlewareRoute.Use(middlewares.ReviewPermissionMiddleware())
	myGameMiddlewareRoute.Use(middlewares.MyGamePermissionMiddleware())
	wishlistMiddlewareRoute.Use(middlewares.WishlistPermissionMiddleware())
	developerMiddlewareRoute.Use(middlewares.DeveloperPermissionMiddleware())
	publisherMiddlewareRoute.Use(middlewares.PublisherPermissionMiddleware())
	devMemberMiddlewareRoute.Use(middlewares.DevMemberPermissionMiddleware())
	pubMemberMiddlewareRoute.Use(middlewares.PubMemberPermissionMiddleware())

	// Authentication & Profile
	r.POST("/register", controllers.Register)
	r.POST("/login", controllers.Login)
	jwtAuthMiddlewareRoute.PUT("/my/profile/update", controllers.UpdateProfile)
	jwtAuthMiddlewareRoute.PUT("/my/profile/change-password", controllers.ChangePassowrd)

	// Roles CRUD
	userMiddlewareRoute.GET("/roles", controllers.GetAllRoles)
	userMiddlewareRoute.GET("/role/:roleID", controllers.GetRoleByID)
	userMiddlewareRoute.GET("/role/:roleID/users", controllers.GetUsersByRoleID)
	roleMiddlewareRoute.POST("/role", controllers.CreateRole)
	roleMiddlewareRoute.PUT("role/:roleID", controllers.UpdateRole)
	roleMiddlewareRoute.DELETE("role/:roleID", controllers.DeleteRole)

	// User CRUD
	userMiddlewareRoute.GET("/users", controllers.GetAllUsers)
	r.GET("/user/:userID", controllers.GetUserByID)
	r.GET("/user/:userID/reviews", controllers.GetReviewsByUserID)
	r.GET("/user/:userID/games", controllers.GetMyGamesByUserID) // Need testing
	userMiddlewareRoute.POST("/user", controllers.CreateUser)
	userMiddlewareRoute.PUT("/user/:userID", controllers.UpdateUserRole)
	userMiddlewareRoute.DELETE("/user/:userID", controllers.DeleteUser)

	// Game CRUD
	r.GET("/games", controllers.GetAllGames)
	r.GET("/game/:gameID", controllers.GetGameByID)
	r.GET("/game/:gameID/reviews", controllers.GetReviewsByGameID)
	gameMiddlewareRoute.POST("/game", controllers.CreateGame)
	gameMiddlewareRoute.PUT("/game/:gameID", controllers.UpdateGame)
	gameMiddlewareRoute.DELETE("/game/:gameID", controllers.DeleteGame)

	// Genre CRUD
	r.GET("/genres", controllers.GetAllGenres)
	r.GET("/genre/:genreID/games", controllers.GetGamesByGenreID) // Need testing
	userMiddlewareRoute.POST("/genre", controllers.CreateGenre)
	userMiddlewareRoute.PUT("/genre/:genreID", controllers.UpdateGenre)
	userMiddlewareRoute.DELETE("/genre/:genreID", controllers.DeleteGenre)

	// Review CRUD
	r.GET("/reviews", controllers.GetAllReviews)
	r.GET("/review/:reviewID", controllers.GetReviewByID)
	r.GET("/review/:reviewID/likes", controllers.GetUserLikesByReviewID)       // Need testing
	r.GET("/review/:reviewID/comments", controllers.GetUserCommentsByReviewID) // Need testing
	reviewMiddlewareRoute.POST("/review", controllers.CreateReview)
	reviewMiddlewareRoute.PUT("/review/:reviewID", controllers.UpdateReview)
	reviewMiddlewareRoute.DELETE("/review/:reviewID", controllers.DeleteReview)

	// MyGame CRUD
	myGameMiddlewareRoute.GET("/my/games", controllers.GetAllMyGames)
	myGameMiddlewareRoute.GET("/my/game/:myGameID", controllers.GetGameByMyGameID) // Need testing
	myGameMiddlewareRoute.POST("/my/game", controllers.CreateMyGame)
	myGameMiddlewareRoute.DELETE("/my/game/:myGameID", controllers.DeleteMyGame)

	// Wishlist CRUD
	wishlistMiddlewareRoute.GET("/my/wishlists", controllers.GetAllMyWishlists)
	wishlistMiddlewareRoute.GET("/my/wishlist/:myWishlistID", controllers.GetGameByMyWishlistID) // Need testing
	wishlistMiddlewareRoute.POST("/my/wishlist", controllers.CreateMyWishlist)
	wishlistMiddlewareRoute.DELETE("/my/wishlist/:myWishlistID", controllers.DeleteMyWishlist)

	// Developer CRUD
	userMiddlewareRoute.GET("/developers", controllers.GetAllDevelopers)
	jwtAuthMiddlewareRoute.GET("/developer/:devID", controllers.GetDeveloperByID)
	jwtAuthMiddlewareRoute.GET("/developer/:devID/users", controllers.GetUsersByDeveloperID) // Need testing
	userMiddlewareRoute.POST("/developer", controllers.CreateDeveloper)
	developerMiddlewareRoute.PUT("/developer/:devID", controllers.UpdateDeveloper)
	userMiddlewareRoute.DELETE("/developer/:devID", controllers.DeleteDeveloper)

	// Publisher CRUD
	userMiddlewareRoute.GET("/publishers", controllers.GetAllPublishers)
	jwtAuthMiddlewareRoute.GET("/publisher/:pubID", controllers.GetPublisherByID)
	jwtAuthMiddlewareRoute.GET("/publisher/:pubID/users", controllers.GetUsersByPublisherID) // Need testing
	userMiddlewareRoute.POST("/publisher", controllers.CreatePublisher)
	publisherMiddlewareRoute.PUT("/publisher/:pubID", controllers.UpdatePublisher)
	userMiddlewareRoute.DELETE("/publisher/:pubID", controllers.DeletePublisher)

	// DeveloperMember CRUD
	devMemberMiddlewareRoute.POST("/developer-member", controllers.CreateDeveloperMember)
	devMemberMiddlewareRoute.DELETE("/developer-member/:devMemberID", controllers.DeleteDeveloperMember)

	// PublisherMember CRUD
	pubMemberMiddlewareRoute.POST("/publisher-member", controllers.CreatePublisherMember)
	pubMemberMiddlewareRoute.DELETE("/publisher-member/:pubMemberID", controllers.DeletePublisherMember)

	// ReviewLike CRUD
	jwtAuthMiddlewareRoute.POST("/review-like/:reviewID", controllers.CreateReviewLike)
	jwtAuthMiddlewareRoute.DELETE("/review-like/:reviewID", controllers.DeleteReviewLike)

	// ReviewComment CRUD
	jwtAuthMiddlewareRoute.POST("/review-comment/:reviewID", controllers.CreateReviewComment)

	// Swagger
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	return r
}
