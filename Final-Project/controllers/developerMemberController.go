package controllers

import (
	"Final-Project/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	developerMemberInput struct {
		UserID      uint `json:"user_id"`
		DeveloperID uint `json:"developer_id"`
	}
)

// CreateDeveloperMember godoc
// @Summary Create New Developer Member.
// @Description Creating a new Developer Member.
// @Tags DeveloperMember
// @Param Body body developerMemberInput true "the body to create a new Developer Member"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.DeveloperMember
// @Router /developer-member [post]
func CreateDeveloperMember(c *gin.Context) {
	var input developerMemberInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	member := models.DeveloperMember{
		UserID:      input.UserID,
		DeveloperID: input.DeveloperID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&member)

	c.JSON(http.StatusCreated, gin.H{"data": member})
}

// DeleteDeveloperMember godoc
// @Summary Delete one Developer Member.
// @Description Delete a Developer Member by id.
// @Tags DeveloperMember
// @Produce json
// @Param devMemberID path string true "Developer Member id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /developer-member/{devMemberID} [delete]
func DeleteDeveloperMember(c *gin.Context) {
	var member models.DeveloperMember
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&member, c.Param("devMemberID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&member)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
