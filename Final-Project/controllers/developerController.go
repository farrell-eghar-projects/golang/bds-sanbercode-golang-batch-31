package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	developerInput struct {
		Name    string `json:"name"`
		Website string `json:"website"`
		Twitter string `json:"twitter"`
	}
)

// GetAllDevelopers godoc
// @Summary Get all Developer Company.
// @Description Get a list of Developers Company.
// @Tags Developer
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.Developer
// @Router /developers [get]
func GetAllDevelopers(c *gin.Context) {
	var developers []models.Developer
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&developers)

	c.JSON(http.StatusOK, gin.H{"data": developers})
}

// CreateDeveloper godoc
// @Summary Create New Developer.
// @Description Creating a new Developer Company.
// @Tags Developer
// @Param Body body developerInput true "the body to create a new Developer"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Developer
// @Router /developer [post]
func CreateDeveloper(c *gin.Context) {
	var input developerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	developer := models.Developer{
		Name:    input.Name,
		Website: input.Website,
		Twitter: input.Twitter,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&developer)

	c.JSON(http.StatusCreated, gin.H{"data": developer})
}

// GetDeveloperByID godoc
// @Summary Get Developer Company.
// @Description Get a Developer by id.
// @Tags Developer
// @Produce json
// @Param devID path string true "Developer id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Developer
// @Router /developer/{devID} [get]
func GetDeveloperByID(c *gin.Context) {
	var developer models.Developer
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&developer, c.Param("devID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": developer})
}

// GetUsersByDeveloperID godoc
// @Summary Get Users.
// @Description Get all Users by DeveloperID.
// @Tags Developer
// @Produce json
// @Param devID path string true "Developer id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} []models.User
// @Router /developer/{devID}/users [get]
func GetUsersByDeveloperID(c *gin.Context) {
	var devMembers []models.DeveloperMember
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("developer_id = ?", c.Param("devID")).Preload("User").Find(&devMembers).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": devMembers})
}

// UpdateDeveloper godoc
// @Summary Update Developer Company.
// @Description Update Developer Company by id.
// @Tags Developer
// @Produce json
// @Param devID path string true "Developer id"
// @Param Body body developerInput true "the body to update Developer Company"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Developer
// @Router /developer/{devID} [put]
func UpdateDeveloper(c *gin.Context) {
	var input developerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var developer models.Developer
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&developer, c.Param("devID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedDeveloper models.Developer
	updatedDeveloper.Name = input.Name
	updatedDeveloper.Website = input.Website
	updatedDeveloper.Twitter = input.Twitter
	updatedDeveloper.UpdatedAt = time.Now()

	db.Model(&developer).Updates(updatedDeveloper)
	c.JSON(http.StatusOK, gin.H{"data": developer})
}

// DeleteDeveloper godoc
// @Summary Delete one Developer Company.
// @Description Delete a Developer Company by id.
// @Tags Developer
// @Produce json
// @Param devID path string true "Developer id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /developer/{devID} [delete]
func DeleteDeveloper(c *gin.Context) {
	var developer models.Developer
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&developer, c.Param("devID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&developer)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
