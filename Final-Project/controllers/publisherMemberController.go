package controllers

import (
	"Final-Project/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	publisherMemberInput struct {
		UserID      uint `json:"user_id"`
		PublisherID uint `json:"publisher_id"`
	}
)

// CreatePublisherMember godoc
// @Summary Create New Publisher Member.
// @Description Creating a new Publisher Member.
// @Tags PublisherMember
// @Param Body body publisherMemberInput true "the body to create a new Publisher Member"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.PublisherMember
// @Router /publisher-member [post]
func CreatePublisherMember(c *gin.Context) {
	var input publisherMemberInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	member := models.PublisherMember{
		UserID:      input.UserID,
		PublisherID: input.PublisherID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&member)

	c.JSON(http.StatusCreated, gin.H{"data": member})
}

// DeletePublisherMember godoc
// @Summary Delete one Publisher Member.
// @Description Delete a Publisher Member by id.
// @Tags PublisherMember
// @Produce json
// @Param pubID path string true "Publisher Member id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /publisher-member/{pubMemberID} [delete]
func DeletePublisherMember(c *gin.Context) {
	var member models.PublisherMember
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&member, c.Param("pubMemberID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&member)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
