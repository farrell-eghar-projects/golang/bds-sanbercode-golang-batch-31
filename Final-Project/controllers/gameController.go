package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	gameInput struct {
		Name        string    `json:"name"`
		Price       string    `json:"price"`
		ReleaseDate time.Time `json:"release_date"`
	}
)

// GetAllGames godoc
// @Summary Get all Games.
// @Description Get a list of Games.
// @Tags Game
// @Produce json
// @Success 200 {object} []models.Game
// @Router /games [get]
func GetAllGames(c *gin.Context) {
	var games []models.Game
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&games)

	c.JSON(http.StatusOK, gin.H{"data": games})
}

// CreateGame godoc
// @Summary Create New Game.
// @Description Creating a new Game.
// @Tags Game
// @Param Body body gameInput true "the body to create a new Game"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Game
// @Router /game [post]
func CreateGame(c *gin.Context) {
	var input gameInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	game := models.Game{
		Name:        input.Name,
		Price:       input.Price,
		ReleaseDate: input.ReleaseDate,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&game)

	c.JSON(http.StatusCreated, gin.H{"data": game})
}

// GetGameByID godoc
// @Summary Get Game.
// @Description Get a Game by id.
// @Tags Game
// @Produce json
// @Param gameID path string true "Game id"
// @Success 200 {object} models.Game
// @Router /game/{gameID} [get]
func GetGameByID(c *gin.Context) {
	var game models.Game
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&game, c.Param("gameID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": game})
}

// GetReviewsByGameID godoc
// @Summary Get Reviews.
// @Description Get all Reviews by GameID.
// @Tags Game
// @Produce json
// @Param gameID path string true "Game id"
// @Success 200 {object} []models.Review
// @Router /game/{gameID}/reviews [get]
func GetReviewsByGameID(c *gin.Context) {
	var reviews []models.Review
	db := c.MustGet("id").(*gorm.DB)
	if err := db.Where("game_id = ?", c.Param("gameID")).Find(&reviews).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": reviews})
}

// UpdateGame godoc
// @Summary Update Game Company.
// @Description Update Game Company by id.
// @Tags Game
// @Produce json
// @Param gameID path string true "Game id"
// @Param Body body gameInput true "the body to update Game Company"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Game
// @Router /game/{gameID} [put]
func UpdateGame(c *gin.Context) {
	var input gameInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var game models.Game
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&game, c.Param("gameID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedGame models.Game
	updatedGame.Name = input.Name
	updatedGame.Price = input.Price
	updatedGame.ReleaseDate = input.ReleaseDate
	updatedGame.UpdatedAt = time.Now()

	db.Model(&game).Updates(updatedGame)
	c.JSON(http.StatusOK, gin.H{"data": game})
}

// DeleteGame godoc
// @Summary Delete one Game Company.
// @Description Delete a Game Company by id.
// @Tags Game
// @Produce json
// @Param gameID path string true "Game id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /game/{gameID} [delete]
func DeleteGame(c *gin.Context) {
	var game models.Game
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&game, c.Param("gameID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&game)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
