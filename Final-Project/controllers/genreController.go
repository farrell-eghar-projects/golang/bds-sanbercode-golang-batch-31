package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	genreInput struct {
		Name string `json:"name"`
	}
)

// GetAllGenres godoc
// @Summary Get all Genres.
// @Description Get a list of Genres.
// @Tags Genre
// @Produce json
// @Success 200 {object} []models.Genre
// @Router /genres [get]
func GetAllGenres(c *gin.Context) {
	var genres []models.Genre
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&genres)

	c.JSON(http.StatusOK, gin.H{"data": genres})
}

// CreateGenre godoc
// @Summary Create New Genre.
// @Description Creating a new Genre.
// @Tags Genre
// @Param Body body genreInput true "the body to create a new Genre"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Genre
// @Router /genre [post]
func CreateGenre(c *gin.Context) {
	var input genreInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	genre := models.Genre{
		Name: input.Name,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&genre)

	c.JSON(http.StatusCreated, gin.H{"data": genre})
}

// GetGamesByGenreID godoc
// @Summary Get Games.
// @Description Get all Games by GenreID.
// @Tags Genre
// @Produce json
// @Param genreID path string true "Genre id"
// @Success 200 {object} []models.Game
// @Router /genre/{genreID}/games [get]
func GetGamesByGenreID(c *gin.Context) {
	var gameGenres []models.GameGenre
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("genre_id = ?", c.Param("genreID")).Preload("Game").Find(&gameGenres).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": gameGenres})
}

// UpdateGenre godoc
// @Summary Update Genre Company.
// @Description Update Genre Company by id.
// @Tags Genre
// @Produce json
// @Param genreID path string true "Genre id"
// @Param Body body genreInput true "the body to update Genre Company"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Genre
// @Router /genre/{genreID} [put]
func UpdateGenre(c *gin.Context) {
	var input genreInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var genre models.Genre
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&genre, c.Param("genreID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedGenre models.Genre
	updatedGenre.Name = input.Name
	updatedGenre.UpdatedAt = time.Now()

	db.Model(&genre).Updates(updatedGenre)
	c.JSON(http.StatusOK, gin.H{"data": genre})
}

// DeleteGenre godoc
// @Summary Delete one Genre Company.
// @Description Delete a Genre Company by id.
// @Tags Genre
// @Produce json
// @Param genreID path string true "Genre id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /genre/{genreID} [delete]
func DeleteGenre(c *gin.Context) {
	var genre models.Genre
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&genre, c.Param("genreID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&genre)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
