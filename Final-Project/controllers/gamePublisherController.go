package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	gamePublisherInput struct {
		GameID      uint `json:"game_id"`
		PublisherID uint `json:"publisher_id"`
	}
)

func GetAllGamePublishers(c *gin.Context) {
	var gamePubs []models.GamePublisher
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&gamePubs)

	c.JSON(http.StatusOK, gin.H{"data": gamePubs})
}

func CreateGamePublisher(c *gin.Context) {
	var input gamePublisherInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	gamePub := models.GamePublisher{
		GameID:      input.GameID,
		PublisherID: input.PublisherID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&gamePub)

	c.JSON(http.StatusCreated, gin.H{"data": gamePub})
}

func GetGamePublisherByID(c *gin.Context) {
	var gamePub models.GamePublisher
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&gamePub, c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": gamePub})
}

func UpdateGamePublisher(c *gin.Context) {
	var input gamePublisherInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var gamePub models.GamePublisher
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&gamePub, c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedGamePub models.GamePublisher
	updatedGamePub.GameID = input.GameID
	updatedGamePub.PublisherID = input.PublisherID
	updatedGamePub.UpdatedAt = time.Now()

	db.Model(&gamePub).Updates(updatedGamePub)
	c.JSON(http.StatusOK, gin.H{"data": gamePub})
}

func DeleteGamePublisher(c *gin.Context) {
	var gamePub models.GamePublisher
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&gamePub, c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&gamePub)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
