package controllers

import (
	"Final-Project/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	wishlistInput struct {
		GameID uint `json:"game_id"`
	}
)

// GetAllMyWishlists godoc
// @Summary Get all My Wishlists.
// @Description Get a list of My Wishlists.
// @Tags Wishlist
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.Wishlist
// @Router /my/wishlists [get]
func GetAllMyWishlists(c *gin.Context) {
	var myWishlists []models.Wishlist
	db := c.MustGet("db").(*gorm.DB)
	db.Where("user_id = ?", models.LoggedInUser.ID).Find(&myWishlists)

	c.JSON(http.StatusOK, gin.H{"data": myWishlists})
}

// CreateMyWishlist godoc
// @Summary Create New Wishlist.
// @Description Creating a new Wishlist.
// @Tags Wishlist
// @Param Body body wishlistInput true "the body to create a new Wishlist"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Wishlist
// @Router /my/wishlist [post]
func CreateMyWishlist(c *gin.Context) {
	var input wishlistInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	wishlist := models.Wishlist{
		UserID: models.LoggedInUser.ID,
		GameID: input.GameID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&wishlist)

	c.JSON(http.StatusCreated, gin.H{"data": wishlist})
}

// GetGameByMyWishlistID godoc
// @Summary Get Game.
// @Description Get Game by WishlistID.
// @Tags Wishlist
// @Produce json
// @Param myWishlistID path string true "Wishlist id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Wishlist
// @Router /my/wishlist/{myWishlistID} [get]
func GetGameByMyWishlistID(c *gin.Context) {
	var myWishlist models.Wishlist
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Preload("Game").First(&myWishlist, c.Param("myWishlistID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": myWishlist.Game})
}

// DeleteMyWishlist godoc
// @Summary Delete one of my Wishlist.
// @Description Delete a Wishlist by id.
// @Tags Wishlist
// @Produce json
// @Param myWishlistID path string true "Wishlist id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /my/wishlist/{myWishlistID} [delete]
func DeleteMyWishlist(c *gin.Context) {
	var wishlist models.Wishlist
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&wishlist, c.Param("myWishlistID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&wishlist)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
