package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	userUpdateProfile struct {
		Fullname    string    `json:"fullname"`
		Nickname    string    `json:"nickname"`
		Age         int       `json:"age"`
		Gender      string    `json:"gender"`
		DateOfBirth time.Time `json:"date_of_birth"`
	}

	updateUserRoleInput struct {
		RoleID uint `json:"role_id"`
		Leader bool `json:"leader"`
	}
)

// GetAllUsers godoc
// @Summary Get all Users.
// @Description Get a list of Users.
// @Tags User
// @Produce json
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} []models.User
// @Router /users [get]
func GetAllUsers(c *gin.Context) {
	var users []models.User
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&users)

	c.JSON(http.StatusOK, gin.H{"data": users})
}

// CreateUser godoc
// @Summary Create New User.
// @Description Creating a new User.
// @Tags User
// @Produce json
// @Param Body body registerInput true "the body to create a new User"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.User
// @Router /user [post]
func CreateUser(c *gin.Context) {
	var input registerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	u := models.User{
		Email:    input.Email,
		Username: input.Username,
		Password: input.Password,
	}
	db := c.MustGet("db").(*gorm.DB)
	_, err := u.SaveUser(db)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	user := map[string]string{
		"email":    u.Email,
		"username": u.Username,
	}
	c.JSON(http.StatusCreated, gin.H{"message": "You are registered!", "user": user})
}

// GetUserByID godoc
// @Summary Get User.
// @Description Get a User by id.
// @Tags User
// @Produce json
// @Param userID path string true "User id"
// @Success 200 {object} models.User
// @Router /user/{userID} [get]
func GetUserByID(c *gin.Context) {
	var user models.User
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&user, c.Param("userID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": user})
}

// GetReviewsByUserID godoc
// @Summary Get Reviews.
// @Description Get all Reviews by UserID.
// @Tags User
// @Produce json
// @Param userID path string true "User id"
// @Success 200 {object} []models.Review
// @Router /user/{userID}/reviews [get]
func GetReviewsByUserID(c *gin.Context) {
	var reviews []models.Review
	db := c.MustGet("id").(*gorm.DB)
	if err := db.Where("user_id = ?", c.Param("userID")).Find(&reviews).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": reviews})
}

// GetMyGamesByUserID godoc
// @Summary Get MyGames.
// @Description Get all MyGames by UserID.
// @Tags User
// @Produce json
// @Param userID path string true "User id"
// @Success 200 {object} []models.MyGame
// @Router /user/{userID}/games [get]
func GetMyGamesByUserID(c *gin.Context) {
	var myGames []models.MyGame
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("user_id = ?", c.Param("userID")).Preload("Game").Find(&myGames); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": myGames})
}

// UpdateProfile godoc
// @Summary Update User Profile.
// @Description Update User Profile by id.
// @Tags User
// @Produce json
// @Param Body body userUpdateProfile true "the body to update User Profile"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.User
// @Router /my/profile/update [put]
func UpdateProfile(c *gin.Context) {
	var input userUpdateProfile
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var updatedProfile models.User
	updatedProfile.Fullname = input.Fullname
	updatedProfile.Nickname = input.Nickname
	updatedProfile.Age = input.Age
	updatedProfile.Gender = input.Gender
	updatedProfile.DateOfBirth = input.DateOfBirth
	updatedProfile.UpdatedAt = time.Now()

	db := c.MustGet("db").(*gorm.DB)
	db.Model(&models.LoggedInUser).Updates(updatedProfile)
	c.JSON(http.StatusOK, gin.H{"data": models.LoggedInUser})
}

// UpdateUserRole godoc
// @Summary Update User Role.
// @Description Update User Role by UserID.
// @Tags User
// @Produce json
// @Param userID path string true "User id"
// @Param Body body updateUserRoleInput true "the body to update User Role"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.User
// @Router /user/{userID} [put]
func UpdateUserRole(c *gin.Context) {
	var input updateUserRoleInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var user models.User
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&user, c.Param("userID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedUserRole models.User
	updatedUserRole.RoleID = input.RoleID
	updatedUserRole.Leader = input.Leader
	updatedUserRole.UpdatedAt = time.Now()

	db.Model(&user).Updates(updatedUserRole)
	c.JSON(http.StatusOK, gin.H{"data": user})
}

// DeleteUser godoc
// @Summary Delete one User.
// @Description Delete a User by UserID.
// @Tags User
// @Produce json
// @Param userID path string true "User id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /user/{userID} [delete]
func DeleteUser(c *gin.Context) {
	var user models.User
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&user, c.Param("userID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&user)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
