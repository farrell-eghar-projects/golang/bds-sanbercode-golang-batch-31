package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	gameDeveloperInput struct {
		GameID      uint `json:"game_id"`
		DeveloperID uint `json:"developer_id"`
	}
)

func GetAllGameDevelopers(c *gin.Context) {
	var gameDevs []models.GameDeveloper
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&gameDevs)

	c.JSON(http.StatusOK, gin.H{"data": gameDevs})
}

func CreateGameDeveloper(c *gin.Context) {
	var input gameDeveloperInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	gameDev := models.GameDeveloper{
		GameID:      input.GameID,
		DeveloperID: input.DeveloperID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&gameDev)

	c.JSON(http.StatusCreated, gin.H{"data": gameDev})
}

func GetGameDeveloperByID(c *gin.Context) {
	var gameDev models.GameDeveloper
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&gameDev, c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": gameDev})
}

func UpdateGameDeveloper(c *gin.Context) {
	var input gameDeveloperInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var gameDev models.GameDeveloper
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&gameDev, c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedGameDev models.GameDeveloper
	updatedGameDev.GameID = input.GameID
	updatedGameDev.DeveloperID = input.DeveloperID
	updatedGameDev.UpdatedAt = time.Now()

	db.Model(&gameDev).Updates(updatedGameDev)
	c.JSON(http.StatusOK, gin.H{"data": gameDev})
}

func DeleteGameDeveloper(c *gin.Context) {
	var gameDev models.GameDeveloper
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&gameDev, c.Param("id")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&gameDev)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
