package controllers

import (
	"Final-Project/models"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	reviewCommentInput struct {
		Comment string `json:"comment"`
	}
)

// CreateReviewComment godoc
// @Summary Create New Review Comment.
// @Description Creating a new Review Comment.
// @Tags ReviewComment
// @Param reviewID path string true "Review id"
// @Param Body body reviewCommentInput true "the body to create a new Review Comment"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.ReviewComment
// @Router /review-comment/{reviewID} [post]
func CreateReviewComment(c *gin.Context) {
	var input reviewCommentInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	conv, _ := strconv.Atoi(c.Param("reviewID"))

	comment := models.ReviewComment{
		UserID:   models.LoggedInUser.ID,
		ReviewID: uint(conv),
		Comment:  input.Comment,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&comment)

	c.JSON(http.StatusCreated, gin.H{"data": comment})
}
