package controllers

import (
	"Final-Project/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	myGameInput struct {
		GameID uint `json:"game_id"`
	}
)

// GetAllMyGames godoc
// @Summary Get all MyGame.
// @Description Get a list of MyGames.
// @Tags MyGame
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} []models.MyGame
// @Router /my/games [get]
func GetAllMyGames(c *gin.Context) {
	var myGames []models.MyGame
	db := c.MustGet("db").(*gorm.DB)
	db.Where("user_id = ?", models.LoggedInUser.ID).Find(&myGames)

	c.JSON(http.StatusOK, gin.H{"data": myGames})
}

// CreateMyGame godoc
// @Summary Create New MyGame.
// @Description Creating a new MyGame.
// @Tags MyGame
// @Param Body body myGameInput true "the body to create a new MyGame"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.MyGame
// @Router /my/game [post]
func CreateMyGame(c *gin.Context) {
	var input myGameInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	myGame := models.MyGame{
		UserID: models.LoggedInUser.ID,
		GameID: input.GameID,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&myGame)

	c.JSON(http.StatusCreated, gin.H{"data": myGame})
}

// GetGameByMyGameID godoc
// @Summary Get Game.
// @Description Get Game by MyGameID.
// @Tags MyGame
// @Produce json
// @Param myGameID path string true "MyGame id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Game
// @Router /my/game/{myGameID} [get]
func GetGameByMyGameID(c *gin.Context) {
	var myGame models.MyGame
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&myGame, c.Param("myGameID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": myGame.Game})
}

// DeleteMyGame godoc
// @Summary Delete one MyGame.
// @Description Delete a MyGame by id.
// @Tags MyGame
// @Produce json
// @Param myGameID path string true "MyGame id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /my/game/{myGameID} [delete]
func DeleteMyGame(c *gin.Context) {
	var myGame models.MyGame
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&myGame, c.Param("myGameID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&myGame)
	c.JSON(http.StatusOK, gin.H{"data": true})
}
