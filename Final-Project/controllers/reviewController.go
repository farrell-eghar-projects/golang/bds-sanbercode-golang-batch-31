package controllers

import (
	"Final-Project/models"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type (
	reviewInput struct {
		GameID      uint   `json:"game_id"`
		Title       string `json:"title"`
		Description string `json:"description"`
	}
)

// GetAllReviews godoc
// @Summary Get all Reviews.
// @Description Get a list of Reviews.
// @Tags Review
// @Produce json
// @Success 200 {object} []models.Review
// @Router /reviews [get]
func GetAllReviews(c *gin.Context) {
	var reviews []models.Review
	db := c.MustGet("db").(*gorm.DB)
	db.Find(&reviews)

	c.JSON(http.StatusOK, gin.H{"data": reviews})
}

// CreateReview godoc
// @Summary Create New Review.
// @Description Creating a new Review.
// @Tags Review
// @Param Body body reviewInput true "the body to create a new Review"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Produce json
// @Success 200 {object} models.Review
// @Router /review [post]
func CreateReview(c *gin.Context) {
	var input reviewInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"data": err.Error()})
		return
	}

	review := models.Review{
		GameID:      input.GameID,
		Title:       input.Title,
		Description: input.Description,
	}
	db := c.MustGet("db").(*gorm.DB)
	db.Create(&review)

	c.JSON(http.StatusCreated, gin.H{"data": review})
}

// GetReviewByID godoc
// @Summary Get Review.
// @Description Get a Review by id.
// @Tags Review
// @Produce json
// @Param reviewID path string true "Review id"
// @Success 200 {object} models.Review
// @Router /review/{ReviewID} [get]
func GetReviewByID(c *gin.Context) {
	var review models.Review
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&review, c.Param("reviewID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": review})
}

// GetUserLikesByReviewID godoc
// @Summary Get UserLikes.
// @Description Get all UserLikes by ReviewID.
// @Tags Review
// @Produce json
// @Param reviewID path string true "Review id"
// @Success 200 {object} []models.ReviewLike
// @Router /review/{reviewID}/likes [get]
func GetUserLikesByReviewID(c *gin.Context) {
	var likes []models.ReviewLike
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("review_id = ?", c.Param("reviewID")).Preload("User").Find(&likes); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": likes})
}

// GetUserCommentsByReviewID godoc
// @Summary Get UserComments.
// @Description Get all UserComments by ReviewID.
// @Tags Review
// @Produce json
// @Param reviewID path string true "Review id"
// @Success 200 {object} []models.ReviewComment
// @Router /review/{reviewID}/comments [get]
func GetUserCommentsByReviewID(c *gin.Context) {
	var comments []models.ReviewComment
	db := c.MustGet("db").(*gorm.DB)
	if err := db.Where("review_id", c.Param("reviewID")).Find(&comments); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": comments})
}

// UpdateReview godoc
// @Summary Update Review.
// @Description Update Review by id.
// @Tags Review
// @Produce json
// @Param reviewID path string true "Review id"
// @Param Body body reviewInput true "the body to update Review"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} models.Review
// @Router /review/{reviewID} [put]
func UpdateReview(c *gin.Context) {
	var input reviewInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	var review models.Review
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&review, c.Param("reviewID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var updatedReview models.Review
	updatedReview.GameID = input.GameID
	updatedReview.Title = input.Title
	updatedReview.Description = input.Description
	updatedReview.UpdatedAt = time.Now()

	db.Model(&review).Updates(updatedReview)
	c.JSON(http.StatusOK, gin.H{"data": review})
}

// DeleteReview godoc
// @Summary Delete one Review.
// @Description Delete a Review by id.
// @Tags Review
// @Produce json
// @Param reviewID path string true "Review id"
// @Param Authorization header string true "Authorization. How to input in swagger : 'Bearer <insert_your_token_here>'"
// @Security BearerToken
// @Success 200 {object} map[string]boolean
// @Router /review/{reviewID} [delete]
func DeleteReview(c *gin.Context) {
	var review models.Review
	db := c.MustGet("db").(*gorm.DB)
	if err := db.First(&review, c.Param("reviewID")).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	db.Delete(&review)
	c.JSON(http.StatusBadRequest, gin.H{"data": true})
}
